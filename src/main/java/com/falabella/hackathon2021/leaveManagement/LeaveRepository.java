package com.falabella.hackathon2021.leaveManagement;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LeaveRepository extends JpaRepository<LeaveEntity, UUID> {

    List<LeaveEntity> findByEmployeeId(String employeeId);
}
