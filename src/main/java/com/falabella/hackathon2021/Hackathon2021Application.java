package com.falabella.hackathon2021;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hackathon2021Application {

    public static void main(String[] args) {
        SpringApplication.run(Hackathon2021Application.class, args);
    }

}
