package com.falabella.hackathon2021.fcsr.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionList {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    String transactionId;

    String date;
    String debit;
    String credit;
}
